﻿$(document).ready(function() {
	$('#response, #status, #config').hide();
	var api_key = '2e7e7bc5febf86c7c1762a04872e489b',
		// the following variables define poster quality, later used while getting the poster image
		normal = 'w500',
		low = 'w342',
		hd = 'original',
		
		WWidth = $(window).width();
	
	// variable doms (when declaring a target only once using a variable, jquery works fast)
	var $response = $('#response'),
	$status = $('#status'),
	$poster = $('#poster');
	
	// function to prevent blank user data
	function ValidateSearch(target) {
		valid = (target != '') ? 1:0;
		return valid
	}
	
	// loads css dynamically
	function loadCSS(location) {
		css = $('<link>');
		css.attr = {
			rel: 'stylesheet',
			href: location
		}
		$('head').append(css);
	}
	
	// formats the userinputted string correctly, replaces blank space with '+'
	function formatStr(string) {
		format = string.replace(' ', '+');
		return format;
	}
	
	// the function for showing error 
	function errorMsg(tar, message) {
		tar.text(message).css({'color' : 'red', 'fontSize' : '1.3em'}).fadeIn(600);
	}
	
	// default parameters for ajax requests; $.ajaxSetup is used to set default PARAM values for ajax requests
	$.ajaxSetup({
		timeout: 30000
	});
	
	// gets the configuration (base-url)
	$.get('http://api.themoviedb.org/3/configuration?api_key=' + api_key, function (data) {
		res = data.images.base_url;
		$('#config').attr('data-url', res);
	});
	
	// the main user request processing starts here 
	$('#search_bar').submit(function(evt) {
		evt.preventDefault();
		$status.text($status.attr('data-default')); // sets STATUS message to it's default value
		$status.css('color', ''); // sets STATUS font color to it's default value
		    
		$response.slideUp(300);
		$status.fadeIn(800);
		var user_input = $('#search_bar #userinput').val(),
		
		isValid = ValidateSearch(user_input); // Validates the search, prevents further processing if search bar is blank, see function ValidateSearch
		
		if (isValid != 0) {
			search = formatStr(user_input).toLowerCase(); // the final formatted string of the user input 
					
			// the main ajax request for loading the image file
			$.get('http://api.themoviedb.org/3/search/movie?api_key=' + api_key + '&query=' + search, function(data) {
				if (data.total_results > 0) {
					response = {
						title : data.results[0].title,
						year : data.results[0].release_date.slice(0, 4),
						img : data.results[0].poster_path
					}
					// adds data into the main page
					$('figcaption hgroup:first-child h2').text(response['title']);
					$('figcaption hgroup:last-child h2').text(response['year']);
					$poster.attr('src', $('#config').attr('data-url') + ((WWidth > 550) ? normal : low) + response['img']); // loads NORMAL image above res 550, loads LOW image less res 550
					
					// the data becomes visible after the image is loaded completely
					$poster.load(function() {
						$status.slideUp(300, function() {
							$response.delay(310).slideDown(400); // the delay is to cope up with the previous response hideout time
						});
					});
				} else {
					errorMsg($status, 'Sorry, movie not found');
				}
			}).fail(function(jqXHR, textStatus, errorThrown) { // jqXHR param = returns various errors such as 404 not found; textStatus (necessary) = returns text messages such as "timeout", erorrThrown = param unknown and not necessary at all
				(textStatus === "timeout") ? errorMsg($status, 'Sorry, the request could be completed. Timeout error occured') : errorMsg($status, 'Sorry, an error occured while trying to get the poster')
			}); // fail function for the POSTER IMAGE request

		}
		
		else {
			errorMsg($status, 'Please enter a valid movie title'); // shows the message if search bar is empty
		}
	}); // end of submit function
}); // end of code: document.ready